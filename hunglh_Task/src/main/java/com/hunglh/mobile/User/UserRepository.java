package com.hunglh.mobile.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository  extends CrudRepository<User,String> {
}
