package com.hunglh.mobile.Cart;

import org.springframework.data.repository.CrudRepository;

public interface CartRepository  extends CrudRepository<Cart,Integer> {
}
